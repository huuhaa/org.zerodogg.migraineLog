// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'genericwidgets.dart';
import 'datatypes.dart';
import 'colors.dart';

/// The welcome screen
class MigraineLogWelcome extends StatelessWidget {
  String _welcomeBodyText() {
    return Intl.message(
        'Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the "+"-button.\n\nFor more help, select "Help" in the menu at the top right of the screen.');
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      MDTextBox(text: _welcomeBodyText()),
    ]);
  }
}

/// The help screen
class MigraineLogHelp extends StatelessWidget {
  String _helpMessage() {
    return Intl.message('Help',
        desc: "Menu entry that triggers display of the help screen");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_helpMessage()),
      ),
      // The main editor, with the date, strength and meds taken selectors
      body: MigraineLogHelpText(),
    );
  }
}

class MigraineLogHelpText extends StatelessWidget {
  String _calendarIconHeaderMessage() {
    return Intl.message("Icons in the calendar",
        desc: "Header in the help screen");
  }

  String _colourHeaderMessage() {
    return Intl.message("Colours", desc: "Header in the help screen");
  }

  String _calendarUnderlineDescriptionMessage() {
    return Intl.message(
        "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.");
  }

  String _calendarCircleDescriptionMessage() {
    return Intl.message(
        "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.");
  }

  String _calendarNoteMarkerDescriptionMessage() {
    return Intl.message(
        "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.");
  }

  String _colourMessage() {
    return Intl.message(
        "Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.");
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Consumer<MigraineLogConfig>(
        builder: (context, config, _) => Column(children: [
          MigraineLogHeader(text: _calendarIconHeaderMessage()),
          Table(
            columnWidths: {
              0: FixedColumnWidth(35),
            },
            children: [
              TableRow(
                children: [
                  SizedBox(
                    width: 35,
                    height: 35,
                    child: Container(
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 3.0, color: MDColors.migraine))),
                    ),
                  ),
                  Text(_calendarUnderlineDescriptionMessage()),
                ],
              ),
              TableRow(children: [
                Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                Container(),
              ]),
              TableRow(
                children: [
                  SizedBox(
                    width: 35,
                    height: 35,
                    child: Container(
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border:
                              Border.all(width: 3.0, color: MDColors.migraine)),
                    ),
                  ),
                  Text(_calendarCircleDescriptionMessage()),
                ],
              ),
              if (config.displayNoteMarker)
                TableRow(children: [
                  Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                  Container(),
                ]),
              if (config.displayNoteMarker)
                TableRow(
                  children: [
                    SizedBox(
                      width: 35,
                      height: 35,
                      child: Container(
                        margin: EdgeInsets.all(5.0),
                        child: Stack(
                          children: [
                            Positioned(
                              child: Container(
                                  color: Colors.white60,
                                  height: 3.0,
                                  width: 3.0),
                              top: 4.0,
                              left: 20.0,
                            ),
                            Center(
                              child: Text(
                                "1",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .color,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Text(_calendarNoteMarkerDescriptionMessage()),
                  ],
                ),
            ],
          ),
          MigraineLogHeader(text: _colourHeaderMessage()),
          Text(_colourMessage()),
          MDTextBox(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 5,
              ),
              style: TextStyle(color: MDColors.strongMigraine),
              text: config.messages.strongMigraineMessage()),
          MDTextBox(
              padding: EdgeInsets.symmetric(vertical: 5),
              style: TextStyle(color: MDColors.migraine),
              text: config.messages.migraineMesssage()),
          MDTextBox(
              padding: EdgeInsets.symmetric(vertical: 5),
              style: TextStyle(color: MDColors.headache),
              text: config.messages.headacheMessage()),
        ]),
      ),
    );
  }
}
