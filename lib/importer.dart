// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'datatypes.dart';
import 'definitions.dart';
import 'package:meta/meta.dart';
import 'package:file_picker/file_picker.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'dart:io';

class MigraineLogImporter {
  MigraineLogImporter(this.list);
  MigraineList list;

  @visibleForTesting
  MLIIntermediateResult getJSONFromHTMLString(String content) {
    if (!content.startsWith('<!DOCTYPE html>')) {
      return MLIIntermediateResult.failure(
        error: "Does not start with HTML doctype",
        errorType: MigraineLogImporterError.wrongFiletype,
      );
    }
    if (!content.contains("migraineLogData")) {
      return MLIIntermediateResult.failure(
        error: "Does not contain migraineLogData",
        errorType: MigraineLogImporterError.wrongFiletype,
      );
    }

    for (var line in content.split("\n")) {
      if (line.startsWith("{")) {
        return MLIIntermediateResult.success(result: line);
      }
    }
    return MLIIntermediateResult.failure(
      error: "JSON not found",
      errorType: MigraineLogImporterError.corruptFile,
    );
  }

  @visibleForTesting
  MLIIntermediateResult getMapFromHTMLString(String str) {
    var json = getJSONFromHTMLString(str);
    Map data;
    if (!json.success) {
      return json;
    }
    try {
      data = jsonDecode(json.result);
    } catch (_) {
      return MLIIntermediateResult.failure(
        error: "Failed to parse JSON",
        errorType: MigraineLogImporterError.corruptFile,
      );
    }
    return MLIIntermediateResult.success(result: data);
  }

  @visibleForTesting
  MigraineEntry parseSingleEntry(Map parseEntry, DateTime date) {
    var entry = MigraineEntry.fromMap(parseEntry);
    entry.date = date;
    return entry;
  }

  @visibleForTesting
  MLIIntermediateResult performImport(Map data) {
    if (data['data'] == null || !(data['data'] is Map)) {
      return MLIIntermediateResult.failure(
        error: "Data missing",
        errorType: MigraineLogImporterError.corruptFile,
      );
    }
    if (data['exportVersion'] != DATAVERSION) {
      return MLIIntermediateResult.failure(
        error: "Invalid data version",
        errorType: MigraineLogImporterError.unsupportedVersion,
      );
    }
    List<MigraineEntry> entries = [];
    for (var sub in data['data'].keys) {
      for (var date in data['data'][sub].keys) {
        var parsedDate = DateTime.parse(date);
        entries.add(parseSingleEntry(data['data'][sub][date], parsedDate));
      }
    }
    for (var entry in entries) {
      list.set(entry);
    }
    return MLIIntermediateResult.success(result: data);
  }

  MigraineLogImporterResult importFromString(String str) {
    var json = getMapFromHTMLString(str);
    Map data;
    if (!json.success) {
      return json;
    }
    data = json.result as Map;
    try {
      MigraineLogImporterResult importStatus = performImport(data);
      return importStatus;
    } catch (_) {
      return MigraineLogImporterResult.failure(
          error: "General error during import");
    }
  }
}

class MigraineLogImportUI extends MigraineLogImporter {
  MigraineLogImportUI(list) : super(list);

  String importSuccessMessage() {
    return Intl.message("Successfully imported data");
  }

  String corruptErrorMessage() {
    return Intl.message("The file is corrupt and can not be imported");
  }

  String wrongFiletypeMessage() {
    return Intl.message(
        "This file does not appear to be a Migraine Log (html) file");
  }

  String unsupportedVersionMessage() {
    return Intl.message(
        "This file is from a newer version of Migraine Log. Upgrade the app first.");
  }

  String unknownErrorMessage() {
    return Intl.message("An unknown error ocurred during import.");
  }

  String importFailedMessage() {
    return Intl.message("Import failed:",
        desc: "Will contain information about why after the :");
  }

  /// Initiates a platform-specific file picker using FilePicker.platform.
  /// This is a method so that it can be overridden for tests.
  @visibleForTesting
  Future<String> initiateFilePicker() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();
    if (result != null) {
      return result.files.single.path;
    }
    return null;
  }

  Future<bool> getFromFilepicker(BuildContext context) async {
    var path = await initiateFilePicker();
    if (path == null) {
      return false;
    }
    File file = File(path);
    var status = importFromString(file.readAsStringSync());
    if (status.success) {
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 6),
        content: Text(importSuccessMessage()),
      ));
      return true;
    }
    String errorMessage;
    switch (status.errorType) {
      case MigraineLogImporterError.corruptFile:
        {
          errorMessage = corruptErrorMessage();
        }
        break;
      case MigraineLogImporterError.wrongFiletype:
        {
          errorMessage = wrongFiletypeMessage();
        }
        break;
      case MigraineLogImporterError.unsupportedVersion:
        {
          errorMessage = unsupportedVersionMessage();
        }
        break;

      case MigraineLogImporterError.unknown:
        {
          errorMessage = unknownErrorMessage();
        }
        break;
    }
    Scaffold.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 10),
        content: Text(importFailedMessage() + ' ' + errorMessage),
      ),
    );
    return false;
  }
}

enum MigraineLogImporterError {
  wrongFiletype,
  corruptFile,
  unsupportedVersion,
  unknown
}

class MigraineLogImporterResult {
  MigraineLogImporterResult.failure(
      {@required String error, MigraineLogImporterError errorType}) {
    _error = error;
    _success = false;
    if (errorType != null) {
      _errorType = errorType;
    } else {
      _errorType = MigraineLogImporterError.unknown;
    }
  }

  /// Syntactic sugare, the same as MigraineLogImporterResult().
  MigraineLogImporterResult.success();
  MigraineLogImporterResult();

  bool _success = true;
  String _error;
  MigraineLogImporterError _errorType;

  MigraineLogImporterError get errorType => _errorType;
  bool get success => _success;
  String get error => _error;
}

@visibleForTesting
class MLIIntermediateResult extends MigraineLogImporterResult {
  dynamic result;
  MLIIntermediateResult.failure(
      {@required error, @required MigraineLogImporterError errorType}) {
    _error = error;
    _success = false;
    _errorType = errorType;
  }
  MLIIntermediateResult.success({@required this.result});
}
