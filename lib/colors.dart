// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

/// Colorscheme for Migraine Log
class MDColors {
  // Prevent instantiation
  MDColors._();

  /// The color used to highlight strong migraines
  static const Color strongMigraine = Colors.red;

  /// The color used to highlight migraines
  static const Color migraine = Colors.orange;

  /// The color used to highlight headaches
  static const Color headache = Colors.teal;

  /// The color used for areas where "no headache registered" should be its own color
  static const Color nothingRegistered = Colors.grey;

  /// Background for error messages
  static const Color errorBackground = Colors.red;

  /// The "primary swatch" which defines the material colorscheme
  static const Color primarySwatch = Colors.blue;

  /// The color used to accent buttons etc.
  static const Color accentColor = Colors.blueGrey;

  /// The color used for links
  static const Color linkColor = Colors.blue;

  /// The color used to highlight the current day in the calendar
  static Color get calendarToday => Colors.brown[300];
}
