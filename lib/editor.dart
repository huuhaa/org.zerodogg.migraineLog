// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'datatypes.dart';
import 'genericwidgets.dart';
import 'package:provider/provider.dart';
import 'colors.dart';
import 'definitions.dart';
import 'localehack.dart';

/// An editor for MigraineEntry
class MigraineLogEditor extends StatefulWidget {
  MigraineLogEditor({Key key, @required this.entry, this.editMode});

  /// editMode is defined by this being non-null. If non-null then this date
  /// will be used as the "original" date for an entry, which we need to be
  /// able to display warnings to the user about moving the event.
  final DateTime editMode;

  /// The MigraineEntry to edit
  final MigraineEntry entry;
  @override
  _MigraineLogEditorState createState() => _MigraineLogEditorState();
}

class _MigraineLogEditorState extends State<MigraineLogEditor> {
  String editMessage() {
    return Intl.message("Edit", desc: "Button that edits a selected entry");
  }

  String registerMessage() {
    return Intl.message(
      "Register",
      desc: "Displayed as the header when registering a new migraine attack",
    );
  }

  String chooseAStrengthMessage() {
    return Intl.message("You must select a strength",
        desc:
            "Notice that appears if the user tries to save an entry without selecting a strength");
  }

  String saveMessage() {
    return Intl.message("Save");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        /// Display editMessage as the header title if editMode, otherwise display registerMessage
        widget.editMode == null ? registerMessage() : editMessage(),
      )),
      // The main editor, with the date, strength and meds taken selectors
      body: ChangeNotifierProvider.value(
          value: widget.entry,
          child: ListView(
            children: <Widget>[
              MigraineDateSelector(
                editMode: widget.editMode,
              ),
              MigraineStrengthSelector(),
              MigraineMedsTakenSelector(),
              Consumer<MigraineEntry>(
                  builder: (context, entry, _) =>
                      MigraineNoteEditor(entry: entry)),
              // The FAB might overlap the MigraineNoteEditor, so we add some
              // padding to compensate
              FABContentPadding(),
            ],
          )),

      /// Our floatingActionButton is used to save an entry
      floatingActionButton: Builder(
        builder: (BuildContext context) => FloatingActionButton(
          onPressed: () {
            var entry = widget.entry;
            if (entry.strength == null) {
              // Show a SnackBar with an error if the user hasn't chosen a strength
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(chooseAStrengthMessage()),
                  backgroundColor: MDColors.errorBackground));
            } else {
              // If we're saving in editMode then we remove the old one first
              if (widget.editMode != null) {
                entry.parentList.remove(widget.editMode);
              }
              // Save it
              entry.save();
              // Go back
              Navigator.of(context).pop();
            }
          },
          tooltip: saveMessage(),
          child: Icon(Icons.save),
        ),
      ),
    );
  }
}

/// A date selector
class MigraineDateSelector extends StatefulWidget {
  MigraineDateSelector({this.editMode});
  final DateTime editMode;
  @override
  _MigraineDateSelectorState createState() => _MigraineDateSelectorState();
}

class _MigraineDateSelectorState extends State<MigraineDateSelector> {
  /// A formatter for dates using the DateRenderFormat and dateLocale
  final format = DateFormat(DateRenderFormat, LocaleHack.dateLocale);

  /// Function that gets called when the user wants to select a date. Triggers
  /// the platform datepicker.
  void _selectDate(BuildContext context, MigraineEntry entry) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: entry.date,
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != entry.date) {
      entry.date = picked;
    }
  }

  String editingEntryNewDate(String editDate) {
    return Intl.message(
      "You're editing an entry on $editDate. If you change the date then this entry will be moved to the new date.",
      args: [editDate],
      name: 'editingEntryNewDate',
    );
  }

  String overwriteWarning() {
    return Intl.message(
        "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.");
  }

  String dateMessage() {
    return Intl.message("Date");
  }

  String semanticChangeDateMessage() {
    return Intl.message("change date",
        desc:
            "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'");
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      /// Show a header
      MigraineLogHeader(text: dateMessage()),

      /// Add a button that we use to trigger the dateselector
      Consumer<MigraineEntry>(
        builder: (context, entry, _) => RaisedButton(
          onPressed: () => _selectDate(context, entry),
          child: Semantics(
            onTapHint: semanticChangeDateMessage(),
            button: true,
            child: Text(
              format.format(entry.date),
            ),
          ),
        ),
      ),
      // Warn about duplicate entries
      Consumer<MigraineEntry>(builder: (context, entry, _) {
        List<Widget> messages = [];
        if (widget.editMode != null && widget.editMode != entry.date) {
          final String editDate = format.format(widget.editMode);
          messages.add(MDTextBox(text: editingEntryNewDate(editDate)));
        }
        if (entry.parentList.exists(entry.date) &&
            (widget.editMode == null || widget.editMode != entry.date)) {
          messages.add(MDTextBox(text: overwriteWarning()));
        }
        return Column(children: messages);
      }),
    ]);
  }
}

/// A wrapper component used to display text in a particular color
class MigraineStrengthText extends StatelessWidget {
  MigraineStrengthText({this.text, this.color, this.fontSize});
  final String text;
  final Color color;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: FontWeight.bold,
        color: color,
      ),
    );
  }
}

/// A widget that is used to select the strength of a migraine attack
class MigraineStrengthSelector extends StatelessWidget {
  String strengthMessage() {
    return Intl.message("Strength", desc: "Header in the edit/add window");
  }

  String painInfoMessage() {
    return Intl.message(
        "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.");
  }

  String strongMigraineDescription() {
    return Intl.message("Unable to perform most activities",
        desc: "Migraine strength 3/3");
  }

  String migraineDescription() {
    return Intl.message("Unable to perform some activities",
        desc: "Migraine strength 2/3");
  }

  String headacheDescription() {
    return Intl.message("Able to perform most activities",
        desc: "Migraine strength 1/3");
  }

  String closeButtonMessage() {
    return Intl.message("Close",
        desc:
            "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines");
  }

  String whichStrengthText() {
    return Intl.message("Which strength should I choose?",
        desc:
            "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.");
  }

  void showStrengthHelpDialog(
      BuildContext context, MigraineLogConfig config, MigraineEntry entry) {
    const double fontSizeHeader = 18;
    const double fontSizeText = 14;
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(whichStrengthText()),
        contentPadding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 23,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: [
              // Header
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Text(
                  painInfoMessage(),
                  style: TextStyle(fontSize: fontSizeText),
                ),
              ),
              // Strong migraine description
              Padding(
                padding: EdgeInsets.only(bottom: 5),
                child: MigraineStrengthText(
                  text: config.messages.strongMigraineMessage(),
                  color: MDColors.strongMigraine,
                  fontSize: fontSizeHeader,
                ),
              ),
              Text(
                strongMigraineDescription(),
                style: TextStyle(fontSize: fontSizeText),
              ),
              // Migraine description
              Padding(
                padding: EdgeInsets.only(bottom: 5, top: 10),
                child: MigraineStrengthText(
                  text: config.messages.migraineMesssage(),
                  color: MDColors.migraine,
                  fontSize: fontSizeHeader,
                ),
              ),
              Text(
                migraineDescription(),
                style: TextStyle(fontSize: fontSizeText),
              ),
              // Headache description
              Padding(
                padding: EdgeInsets.only(bottom: 5, top: 10),
                child: MigraineStrengthText(
                  text: config.messages.headacheMessage(),
                  color: MDColors.headache,
                  fontSize: fontSizeHeader,
                ),
              ),
              Text(
                headacheDescription(),
                style: TextStyle(fontSize: fontSizeText),
              ),
            ],
          ),
        ),
        actions: [
          TextButton(
            child: Text(closeButtonMessage().toUpperCase()),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<MigraineEntry, MigraineLogConfig>(
        builder: (context, entry, config, _) => Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                /// Display a header
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                    padding: EdgeInsets.only(left: 40),
                    child: Text(
                      strengthMessage(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tooltip(
                    message: whichStrengthText(),
                    child: IconButton(
                        icon: Icon(Icons.help),
                        onPressed: () =>
                            showStrengthHelpDialog(context, config, entry)),
                  ),
                ]),

                /// "Strong migraine"
                RadioListTile<MigraineStrength>(
                    title: MigraineStrengthText(
                        text: config.messages.strongMigraineMessage(),
                        color: MDColors.strongMigraine),
                    value: MigraineStrength.strongMigraine,
                    groupValue: entry.strength,
                    onChanged: (MigraineStrength value) {
                      entry.strength = value;
                    }),

                /// "Migraine"
                RadioListTile<MigraineStrength>(
                  title: MigraineStrengthText(
                    text: config.messages.migraineMesssage(),
                    color: MDColors.migraine,
                  ),
                  value: MigraineStrength.migraine,
                  groupValue: entry.strength,
                  onChanged: (MigraineStrength value) {
                    entry.strength = value;
                  },
                ),

                /// "Headache"
                RadioListTile<MigraineStrength>(
                  title: MigraineStrengthText(
                    text: config.messages.headacheMessage(),
                    color: MDColors.headache,
                  ),
                  value: MigraineStrength.headache,
                  groupValue: entry.strength,
                  onChanged: (MigraineStrength value) {
                    entry.strength = value;
                  },
                ),
              ],
            ));
  }
}

/// A widget used to select which kinds of medications a user has taken
class MigraineMedsTakenSelector extends StatelessWidget {
  String medsTakenMessage() {
    return Intl.message("Medications taken",
        desc: "Header in the add/edit window");
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<MigraineEntry, MigraineLogConfig>(
        builder: (context, entry, config, _) => Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                /// Header
                MigraineLogHeader(text: medsTakenMessage()),

                for (var medication in config.medications.list)
                  CheckboxListTile(
                    title: Text(medication.medication),
                    value: entry.medications.has(medication),
                    onChanged: (bool value) {
                      entry.medications.toggle(medication);
                    },
                  ),
                for (var medication in entry.medications.list
                    .where((entry) => !config.medications.has(entry)))
                  CheckboxListTile(
                    title: Text(medication.medication),
                    value: entry.medications.has(medication),
                    onChanged: (bool value) {
                      entry.medications.toggle(medication);
                    },
                  ),
              ],
            ));
  }
}

class MigraineNoteEditor extends StatefulWidget {
  MigraineNoteEditor({@required this.entry});
  final MigraineEntry entry;

  @override
  _MigraineNoteEditorState createState() => _MigraineNoteEditorState();
}

class _MigraineNoteEditorState extends State<MigraineNoteEditor> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.entry.note;
    _controller.addListener(() {
      widget.entry.note = _controller.text;
    });
  }

  String _noteHeader() {
    return Intl.message("Note");
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MigraineLogHeader(text: _noteHeader()),
        Padding(
          padding: EdgeInsets.only(
            left: MDTextBox.defaultInsets,
            right: MDTextBox.defaultInsets,
            bottom: MDTextBox.defaultInsets,
          ),
          child: TextField(
            controller: _controller,
            keyboardType: TextInputType.multiline,
            minLines: 1,
            maxLines: 5,
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
            ),
          ),
        )
      ],
    );
  }
}
