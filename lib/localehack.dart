// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Locale handling in flutter leaves a lot to be desired. This is a static
/// class that attempts to work around the strangeness, and behave closer to
/// what normal desktop apps do. It also provides support for remapping one
/// locale to another.
class LocaleHack {
  // Prevent instantiation
  LocaleHack._();

  /// Resets LocaleHack. Primarily useful for tests.
  static void reset() {
    _detectedLocale = null;
    _detectedDateLocale = null;
    _detectedFlutterLocale = null;
  }

  /// Remap bokmål to nynorsk
  static Map localeRemap = {"no": "nn", "nb": "nn"};

  /// Remap nynorsk to bokmål for flutter, since flutter has no nynorsk
  static Map flutterLocaleRemap = {
    "nn": "nb",
  };

  /// If all else fails, fall back to english
  static String fallbackLocale = 'en';

  /// Our list of supported NON-ENGLISH-locales. English is used as a fallback
  /// if all else fails.
  static List<String> supportedLocales = ['nn', 'fi', 'de', 'es'];

  /// A map that maps our internal locale (ie. the two-character locale code,
  /// as listed in supportedLocales) to the date locale.
  /// See https://pub.dev/documentation/intl/latest/date_symbol_data_local/dateTimeSymbolMap.html
  /// for a list of the locale codes.
  static Map<String, String> dateLocaleMap = {
    'nn': 'no_NO',
    'es': 'es_ES',
  };

  static String overrideLocale;

  // Cache for locale detection
  static String _detectedLocale;
  // Cache for date locale detection
  static String _detectedDateLocale;
  // Cache for flutter locale detection
  static Locale _detectedFlutterLocale;

  /// Retrieve the Locale object for flutter
  static Locale getFlutterLocale() {
    if (_detectedFlutterLocale != null) {
      return _detectedFlutterLocale;
    }
    String locale = detectLocale();
    if (flutterLocaleRemap[locale] != null) {
      locale = flutterLocaleRemap[locale];
    }
    _detectedFlutterLocale = Locale(locale);
    return _detectedFlutterLocale;
  }

  /// Retrieve a string representation of the locale we should use
  static String detectLocale() {
    if (_detectedLocale != null) {
      return _detectedLocale;
    }
    if (overrideLocale != null) {
      return overrideLocale;
    }
    List locales = WidgetsBinding.instance.window.locales;
    _detectedLocale = fallbackLocale;

    for (var localeRaw in locales.reversed) {
      String locale = localeRaw.languageCode.split('_').first;
      if (localeRemap[locale] != null) {
        locale = localeRemap[locale];
      }
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale == locale) {
          _detectedLocale = locale;
          return locale;
        }
      }
    }
    return _detectedLocale;
  }

  /// Initialize Intl with our detected locale
  static void intlInit() {
    Intl.defaultLocale = detectLocale();
  }

  /// Retrieve the dateLocale that corresponds with the detected locale
  static String get dateLocale {
    if (_detectedDateLocale != null) {
      return _detectedDateLocale;
    }
    String locale = detectLocale();
    _detectedDateLocale = locale;
    if (dateLocaleMap[locale] != null) {
      _detectedDateLocale = dateLocaleMap[locale];
    }
    return _detectedDateLocale;
  }
}
