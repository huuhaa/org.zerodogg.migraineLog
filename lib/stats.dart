// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'definitions.dart';
import 'genericwidgets.dart';
import 'datatypes.dart';
import 'colors.dart';
import 'helpwelcome.dart';
import 'a11y.dart';
import 'i18n.dart';

/// A hack that converts from flutter Color to chart_flutter Color, which for
/// some reason aren't compatible.
charts.Color toChartColor(Color from) {
  return charts.Color(r: from.red, g: from.green, b: from.blue, a: from.alpha);
}

/// A widget that displays statistics from a MigraineList
class MigraineLogStatsViewer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MigraineLogGlobalState>(builder: (context, state, _) {
      var currDate = state.currentCalendarMonth;
      var now = DateTime.now();
      var start = DateTime(currDate.year, currDate.month, 1);
      return SingleChildScrollView(
        child: Consumer<MigraineList>(builder: (context, list, _) {
          /// Retrieve the stats
          var stats = list.headacheStats(
            (dt) {
              return dt.month == start.month && (dt.isBefore(now) || dt == now);
            },
            start,
          );
          if (list.isNotEmpty) {
            return MigraineLogStatsRenderer(
              mode: MigraineLogStatsMode.Month,
              stats: stats,
              month: start,
            );
          } else {
            return MigraineLogWelcome();
          }
        }),
      );
    });
  }
}

class MigraineLogStatsLastNDaysViewer extends StatelessWidget {
  MigraineLogStatsLastNDaysViewer({this.days = 30});

  final int days;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<MigraineList>(builder: (context, list, _) {
        /// Retrieve the stats
        var stats = list.headacheDaysStats(days);
        if (list.entries > 0) {
          return MigraineLogStatsRenderer(
              mode: MigraineLogStatsMode.LastNDays, stats: stats, days: days);
        } else {
          return MigraineLogWelcome();
        }
      }),
    );
  }
}

enum MigraineLogStatsMode {
  LastNDays,
  Month,
}

class MigraineLogStatsRenderer extends StatelessWidget {
  MigraineLogStatsRenderer({
    @required this.mode,
    @required this.stats,
    this.days = 0,
    this.month,
  });

  final MigraineLogStatsMode mode;
  final MigraineLogStatisticsList stats;
  final int days;
  final DateTime month;

  String lastNDaysMessage(int days) {
    return Intl.message(
      "Last $days days",
      args: [days],
      name: 'lastNDaysMessage',
      desc: "days should always be >1, so plural",
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> statsWidgets = [];

    /// Add headers
    switch (mode) {
      case MigraineLogStatsMode.LastNDays:
        {
          statsWidgets.add(MigraineLogHeader(text: lastNDaysMessage(days)));
        }
        break;
      case MigraineLogStatsMode.Month:
        {
          statsWidgets.add(MigraineLogMonthSwitcherWidget());
        }
        break;
    }

    /// Build a pie chart
    statsWidgets.add(SizedBox(
      child: MigraineLogPieChart(list: stats.entries),
      height: 250,
    ));

    // Adds some breathing room between the pie chart and the table
    statsWidgets.add(SizedBox(
      height: 25,
      child: Container(),
    ));

    /// Show the same data, but in a table
    statsWidgets.add(MigraineLogTableStats(stats: stats));

    // This is to add some padding at the bottom, so that a user
    // will always be able to scroll if the FAB overlaps the text
    statsWidgets.add(FABContentPadding());
    return Column(children: statsWidgets);
  }
}

/// A table summarizing a period of time
class MigraineLogTableStats extends StatelessWidget {
  MigraineLogTableStats({this.stats});

  final MigraineLogStatisticsList stats;

  String totalHeadacheDays() {
    return Intl.message("Total headache days",
        desc:
            "Used in a table, will render like 'Total headache days    20 days'");
  }

  String takenMedication() {
    return Intl.message("Taken medication",
        desc: "Used in a table, will render like 'Taken Medication    5 days'");
  }

  String formattedStatsEntry({
    MigraineLogStatisticsEntry entry,
    int days,
    double percentageOfDays,
  }) {
    assert(entry != null || (days != null && percentageOfDays != null));
    days ??= entry.days;
    percentageOfDays ??= entry.percentageOfDays;
    return MLi18nStrings.dayString(days) +
        ' ' +
        ' (' +
        percentageOfDays.round().toString() +
        '%)';
  }

  @override
  Widget build(BuildContext context) {
    var innerPadding = EdgeInsets.symmetric(horizontal: 5, vertical: 10);

    /// Initialize the table
    List<TableRow> entries = [];

    /// Build one summary row for each type of data we have
    for (var entry in stats.headacheEntires) {
      var statsEntry = formattedStatsEntry(entry: entry);

      var semanticsLabel = entry.title + ': ' + statsEntry;
      entries.add(buildAccessibleTableRow(elements: [
        MDTextBox(
          padding: innerPadding,
          text: entry.title,
          semanticsLabel: semanticsLabel,
        ),
        MDTextBox(
          semanticsLabel: semanticsLabel,
          padding: innerPadding,
          text: formattedStatsEntry(entry: entry),
        ),
      ]));
    }

    if (stats.medicationDays > 0) {
      var statsEntry = formattedStatsEntry(
          days: stats.medicationDays,
          percentageOfDays: stats.medicationPercentage);

      var semanticsLabel = takenMedication() + ': ' + statsEntry;

      entries.add(buildAccessibleTableRow(elements: [
        MDTextBox(
          padding: innerPadding,
          text: takenMedication(),
          semanticsLabel: semanticsLabel,
        ),
        MDTextBox(
          padding: innerPadding,
          text: statsEntry,
          semanticsLabel: semanticsLabel,
        ),
      ]));
    }
    if (stats.headacheDays > 0) {
      var statsEntry = formattedStatsEntry(
          days: stats.headacheDays, percentageOfDays: stats.headachePercentage);

      var semanticsLabel = totalHeadacheDays() + ': ' + statsEntry;

      entries.add(buildAccessibleTableRow(elements: [
        MDTextBox(
          padding: innerPadding,
          text: totalHeadacheDays(),
          style: TextStyle(fontWeight: FontWeight.bold),
          semanticsLabel: semanticsLabel,
        ),
        MDTextBox(
          padding: innerPadding,
          text: statsEntry,
          style: TextStyle(fontWeight: FontWeight.bold),
          semanticsLabel: semanticsLabel,
        ),
      ]));
    }

    /// Finally add some padding around the table and build the actual widget
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Table(
        children: entries,
        border: TableBorder(
          horizontalInside: BorderSide(color: Colors.grey),
          bottom: BorderSide(color: Colors.grey),
          top: BorderSide(color: Colors.grey),
        ),
      ),
    );
  }
}

/// A pie chart displaying a period of time
class MigraineLogPieChart extends StatelessWidget {
  final List<MigraineLogStatisticsEntry> list;

  MigraineLogPieChart({this.list});

  charts.Color chartColorMapper(MigraineLogStatisticsEntry statsEntry) {
    switch (statsEntry.strength) {
      case MigraineStrength.migraine:
        {
          return toChartColor(MDColors.migraine);
        }
        break;

      case MigraineStrength.strongMigraine:
        {
          return toChartColor(MDColors.strongMigraine);
        }
        break;

      case MigraineStrength.headache:
        {
          return toChartColor(MDColors.headache);
        }
        break;
    }
    return toChartColor(MDColors.nothingRegistered);
  }

  @override
  Widget build(BuildContext context) {
    List<charts.Series> seriesList = [
      charts.Series<MigraineLogStatisticsEntry, String>(
        id: 'Migraine days',
        domainFn: (MigraineLogStatisticsEntry statsEntry, _) =>
            statsEntry.title,
        measureFn: (MigraineLogStatisticsEntry statsEntry, _) =>
            statsEntry.days,
        colorFn: (MigraineLogStatisticsEntry statsEntry, _) {
          return chartColorMapper(statsEntry);
        },
        data: list,
        // Set a label accessor to control the text of the arc label.
        labelAccessorFn: (MigraineLogStatisticsEntry row, _) => row.title,
      )
    ];
    return charts.PieChart(seriesList,
        animate: false,

        /// Defines how the text labels should be rendered
        defaultRenderer: charts.ArcRendererConfig(arcRendererDecorators: [
          charts.ArcLabelDecorator(
              labelPadding: 0,

              /// Text color+size
              outsideLabelStyleSpec: charts.TextStyleSpec(
                color: toChartColor(Colors.white),
                fontSize: 11,
              ),
              insideLabelStyleSpec: charts.TextStyleSpec(
                color: toChartColor(Colors.white),
                fontSize: 11,
              ),

              /// Label positioning. Use flutter-charts builtin auto
              /// positioning. It's not optimal, but better than forcing
              /// specific positioning on our own.
              labelPosition: charts.ArcLabelPosition.auto)
        ]));
  }
}
