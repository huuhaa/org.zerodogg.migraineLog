// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'localehack.dart';
import 'genericwidgets.dart';
import 'datatypes.dart';
import 'colors.dart';
import 'definitions.dart';

/// A widget used to display individual entries in a MigraineList
class MigraineLogViewer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MigraineLogGlobalState>(builder: (context, state, _) {
      var media = MediaQuery.of(context);
      // The width of the calendar. Defaults to infinity. This gets set to a
      // static width in landscape mode.
      double calWidth = double.infinity;
      // The height of our calendar.
      double calHeight;
      if (media.orientation == Orientation.portrait) {
        // Settings for portrait mode

        // Fetch the theoretical max height of the calendar. This is the height
        // of the viewport, minus 100 pixels for the text.
        var heightAfterTextWidget = media.size.height - 100;
        // For possible height over
        // MigraineLogCalendarWidget.maxCalendarHeight, set it to
        // maxCalendarHeight
        if (heightAfterTextWidget >=
            MigraineLogCalendarWidget.maxCalendarHeight) {
          calHeight = MigraineLogCalendarWidget.maxCalendarHeight;
          // For possible height over under 400, set it to heightAfterTextWidget
        } else {
          calHeight = heightAfterTextWidget;
        }
      } else {
        // Settings for landscape mode

        // We've got two widgets, give them equal space
        calWidth = media.size.width / 2;
        // Give them max height, minus 100 for surrounding widgets.
        calHeight = media.size.height - 100;
      }
      List<Widget> viewerWidgets = [
        /// The calendar
        SizedBox(
          width: calWidth,
          child: MigraineLogCalendarWidget(
            height: calHeight,
            currentDate: state.currentDate,
            onDayPressed: (DateTime dt, _) => state.currentDate = dt,
            onDayLongPressed: (DateTime dt) {
              MigraineList list =
                  Provider.of<MigraineList>(context, listen: false);
              var params = MigraineEditorPathParameters(entry: list.get(dt));
              Navigator.of(context).pushNamed('/editor', arguments: params);
            },
          ),
        ),

        /// The day info
        SizedBox(
          width: calWidth,
          child: AnimatedSwitcher(
            duration: Duration(milliseconds: 250),
            child: MigraineLogDayViewer(
              key: ValueKey(state.currentDate),
              currentDate: state.currentDate,
            ),
          ),
        ),
      ];
      if (media.orientation == Orientation.portrait) {
        return ListView(
          children: viewerWidgets,
        );
      }
      return ListView(
        scrollDirection: Axis.horizontal,
        children: viewerWidgets,
      );
    });
  }
}

/// Generates a text summary of the MigraineEntry on the set date
class MigraineLogDayViewer extends StatelessWidget {
  MigraineLogDayViewer({@required this.currentDate, Key key}) : super(key: key);
  final DateTime currentDate;
  final format = DateFormat(DateRenderFormat, LocaleHack.dateLocale);

  @visibleForTesting
  String tookNoMedsMessage() {
    return Intl.message("took no medication");
  }

  @visibleForTesting
  String tookMedsMessage(String meds) {
    return Intl.message(
      "took $meds",
      args: [meds],
      desc:
          "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
      name: "tookMedsMessage",
    );
  }

  @visibleForTesting
  String notePrefixStrig() {
    return Intl.message("Note:");
  }

  @visibleForTesting
  TextSpan getTextElements(MigraineList list, MigraineLogConfig config) {
    MigraineEntry entry = list.get(currentDate);

    String type;
    String taken = tookNoMedsMessage();
    switch (entry.strength) {
      case MigraineStrength.strongMigraine:
        {
          type = config.messages.strongMigraineMessage();
        }
        break;

      case MigraineStrength.migraine:
        {
          type = config.messages.migraineMesssage();
        }
        break;

      default:
        {
          type = config.messages.headacheMessage();
        }
    }

    if (entry.medications.takenMeds) {
      taken = tookMedsMessage(entry.medications.toString());
    }

    TextSpan generatedTextEntry = TextSpan(text: "$type, $taken.");
    TextSpan dayTextEntry;
    if (entry.note != null && entry.note != "") {
      dayTextEntry = TextSpan(children: [
        generatedTextEntry,
        TextSpan(text: "\n\n"),
        TextSpan(
          text: notePrefixStrig() + ' ',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        TextSpan(text: entry.note),
      ]);
    } else {
      dayTextEntry = generatedTextEntry;
    }
    return dayTextEntry;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<MigraineList, MigraineLogConfig>(
        builder: (context, list, config, _) {
      if (!list.exists(currentDate)) {
        return Container();
      }

      return Row(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(MDTextBox.defaultInsets),
              child: SelectableText.rich(
                TextSpan(children: [
                  TextSpan(
                    text: format.format(currentDate) + "\n\n",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  getTextElements(list, config),
                ]),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ],
      );
    });
  }
}

class MigraineLogCalendarWidget extends StatefulWidget {
  MigraineLogCalendarWidget({
    @required this.onDayPressed,
    @required this.onDayLongPressed,
    @required this.currentDate,
    @required this.height,
  });

  static const double maxCalendarHeight = 410;

  final Function(DateTime, dynamic) onDayPressed;
  final Function(DateTime) onDayLongPressed;
  final double height;
  final DateTime currentDate;
  @override
  _MigraineLogCalendarWidgetState createState() =>
      _MigraineLogCalendarWidgetState();
}

/// Builds our calendar widget
class _MigraineLogCalendarWidgetState extends State<MigraineLogCalendarWidget> {
  // This is used to set the current month displayed in our month switcher
  // widget. We use this because CalendarCarousel can at times be a bit slow to
  // update, and we don't want to display a new month header before the
  // calendar itself has updated. So we override the displayedMonth of
  // MigraineLogMonthSwitcherWidget to currentMonthSwitcher, which itself
  // defaults to state.currentCalendarMonth.
  DateTime currentMonthSwitcher;

  Widget customDayBuilder(
    bool isPrevMonthDay,
    bool isNextMonthDay,
    DateTime day, {
    @required MigraineList list,
    @required BuildContext context,
    @required MigraineLogConfig config,
  }) {
    MigraineEntry entry = list.get(day);
    Color textColor = Theme.of(context).textTheme.bodyText2.color;
    // If this day is before or after the current month, then dim the color
    if (isPrevMonthDay || isNextMonthDay) {
      textColor = textColor.withOpacity(0.25);
    }
    // If this day has a headache, add decoration
    if (entry != null) {
      List<Widget> stack = [];

      var color = MDColors.headache;
      if (entry.strength == MigraineStrength.migraine) {
        color = MDColors.migraine;
      } else if (entry.strength == MigraineStrength.strongMigraine) {
        color = MDColors.strongMigraine;
      }
      // Dim the colour if we it's in the previous month
      if (isPrevMonthDay || isNextMonthDay) {
        color = color.withOpacity(0.25);
      }

      BoxDecoration decoration;
      // Highlight with a circle if the user has taken meds
      if (entry.medications.takenMeds) {
        decoration = BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(width: 3.0, color: color));
        // Highlight with an underline if the user hasn't taken meds
      } else {
        decoration = BoxDecoration(
            border: Border(bottom: BorderSide(width: 3.0, color: color)));
      }
      stack.add(
        Center(
          child: Text(
            day.day.toString(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: textColor,
            ),
          ),
        ),
      );
      if (config.displayNoteMarker && entry.hasNote) {
        stack.add(Positioned(
          child: Container(color: Colors.white60, height: 3.0, width: 3.0),
          top: 4.0,
          left: 20.0,
        ));
      }
      return Container(
          margin: EdgeInsets.all(5.0),
          decoration: decoration,
          child: Stack(children: stack));
    } else {
      return Center(
          child: Text(
        day.day.toString(),
        style: TextStyle(color: textColor),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    // This sets the vertical scroll physics for the calendar. It needs room
    // to scroll if the calendar is higher than the viewport. The logic here
    // is that if we've decided on a height of 400 or greater for the
    // callendar, then we assume all of it is displayed and don't allow it to
    // vertically scroll.
    ScrollPhysics verticalPhysics;
    // This configures the calendar to show weeks dynamically or to always show
    // six weeks. We use the same logic as for verticalPhysics: if our height
    // is >= maxCalendarHeight, then we assume enough room to always show six
    // weeks.  Otherwise, room is restricted, so we let the calendar widget
    // decide.
    bool alwaysShowSixWeeks;

    // verticalPhysics and alwaysShowSixWeeks detection. We enable physics if
    // there's a chance we're smaller than the space we need to display
    // everything. Otherwise it gets disabled. We also always show six weeks if
    // there's room for that, since it makes the whole UI jump a lot less.
    if (widget.height >= MigraineLogCalendarWidget.maxCalendarHeight) {
      verticalPhysics = NeverScrollableScrollPhysics();
      alwaysShowSixWeeks = true;
    } else {
      verticalPhysics = AlwaysScrollableScrollPhysics();
      alwaysShowSixWeeks = false;
    }
    return Consumer3<MigraineList, MigraineLogGlobalState, MigraineLogConfig>(
        builder: (context, list, state, config, _) {
      // Initialize currentMonthSwitcher
      currentMonthSwitcher ??= state.currentCalendarMonth;

      return Column(
        children: [
          MigraineLogMonthSwitcherWidget(displayedMonth: currentMonthSwitcher),
          SizedBox(height: 10),
          CalendarCarousel(
            // Hide the Header
            showHeader: false,
            // Change state.currentCalendarMonth and update currentMonthSwitcher
            // onCalendarChanged
            onCalendarChanged: (DateTime dt) {
              state.currentCalendarMonth = dt;
              setState(() => currentMonthSwitcher = dt);
            },
            // The currently displayed month is based upon this value
            // XXX: This is an undocumented setting. Found in the example
            // project for flutter_calendar_carousel:
            // https://github.com/dooboolab/flutter_calendar_carousel/blob/master/example/lib/main.dart
            targetDateTime: state.currentCalendarMonth,
            // Don't permit selecting dates after today
            maxSelectedDate: DateTime.now(),
            // Default date
            selectedDateTime: widget.currentDate,
            // Sets the height (minus the header)
            height:
                widget.height - (MigraineLogMonthSwitcherWidget.height + 10),
            // Monday is the first day of the week
            firstDayOfWeek: 1,
            // Use our accentColor as the selected button color
            selectedDayButtonColor: MDColors.accentColor,
            // The color for "today" when today is not the active day
            todayButtonColor: MDColors.calendarToday,
            // Event handler when the user selects a day
            onDayPressed: (DateTime dt, _) {
              // Switch month when pressing dates on prev/next month
              if (dt.month != currentMonthSwitcher.month) {
                state.currentCalendarMonth = dt;
              }
              // Call the provided event handler
              widget.onDayPressed(dt, _);
            },
            // Event handler when a user long presses on a day
            onDayLongPressed: (DateTime dt) {
              if (!DateTime.now().isBefore(dt)) {
                // Switch month when pressing dates on prev/next month
                if (dt.month != currentMonthSwitcher.month) {
                  state.currentCalendarMonth = dt;
                }
                // Call the provided event handler
                widget.onDayLongPressed(dt);
              }
            },
            // If we want to always show six weeks in the calendar or not
            staticSixWeekFormat: alwaysShowSixWeeks,
            // XXX: This is an undocumented setting. Found in this related issue:
            // https://github.com/dooboolab/flutter_calendar_carousel/issues/135
            customGridViewPhysics: verticalPhysics,
            // The locale, as chosen by dateLocale
            locale: LocaleHack.dateLocale,
            // What the default text color should be
            weekdayTextStyle: TextStyle(color: Colors.white),
            // The builder of the inner element of individual dates. This is used
            // to highlight days with migraine attacks.
            customDayBuilder: (
              bool isSelectable,
              int index,
              bool isSelectedDay,
              bool isToday,
              bool isPrevMonthDay,
              TextStyle textStyle,
              bool isNextMonthDay,
              bool isThisMonthDay,
              DateTime day,
            ) =>
                customDayBuilder(isPrevMonthDay, isNextMonthDay, day,
                    list: list, context: context, config: config),
          ),
        ],
      );
    });
  }
}
