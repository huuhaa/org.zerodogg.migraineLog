Migreenilokin avulla voit helposti kirjata migreenikohtauksesi ja muut päänsärkysi. Sovellus on suunniteltu tekemään seurannasta mahdollisimman helppoa, pakollisia tietoja on vain päivämäärä ja säryn voimakkuus. Halutessasi voit lisätä  tietoja, kuten mitä lääkkeitä otit sekä muistiinpanoja migreenikohtauksesta. Tämän sovelluksen avulla voit seurata migreeniäsi ja se auttaa sinua ja lääkäriäsi löytämään sinulle sopivan hoidon.

Käyttöliittymä on suunniteltu migreeniä sairastaville ja siinä käytetään pehmeitä ja tummia värejä, jotta voit käyttää sitä myös migreenikohtauksen aikana.

Migreeniloki kunnioittaa yksityisyyttäsi eikä pyydä verkkoyhteyttä, joten tietosi eivät lähde laitteestasi, ellet nimenomaisesti käytä sovelluksen vientitoimintoa.
