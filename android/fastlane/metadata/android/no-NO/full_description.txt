Migrenelogg let deg enkelt loggføre migreneanfall (eller andre hodepineanfall). Den er laga for å gjere det så enkelt som mogleg, og krev berre ein dato og styrken på eit anfall, medan du kan leggje til valfri infromasjon som kva medisiner du nytta og eventuelle notat viss du vil. Dette let deg overvake migrenen din og kan bidra til at du og legen din kan finne den rette behandlinga for deg.

Brukargrensesnittet er tilpassa personar med migrene, den nyttar mjuke, mørke fargar sånn at du kan nytte den når du har migrene.

Migrenelogg respekterer personvernet ditt og ber ikkje om tilgang til nettverket, sånn at du kan vere trygg på at informasjonen din ikkje forlet eininga di med mindre du eksplisitt eksporterer den.
