// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:MigraineLog/definitions.dart';

void main() {
  test('MigraineStrengthConverter.humanNumeric', () {
    expect(MigraineStrength.headache.humanNumeric, 1);
    expect(MigraineStrength.migraine.humanNumeric, 2);
    expect(MigraineStrength.strongMigraine.humanNumeric, 3);
  });
  test('MigraineStrengthConverter.fromNumber', () {
    expect(MigraineStrengthConverter.fromNumber(1), MigraineStrength.headache);
    expect(MigraineStrengthConverter.fromNumber(2), MigraineStrength.migraine);
    expect(MigraineStrengthConverter.fromNumber(3),
        MigraineStrength.strongMigraine);
  });
}
