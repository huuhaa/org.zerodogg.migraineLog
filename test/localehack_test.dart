// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:matcher/matcher.dart';
import 'package:MigraineLog/localehack.dart';
import 'package:flutter/material.dart' as flutter_mat;
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';

void main() {
  setUp(() async {
    LocaleHack.reset();
  });
  var locales = List.from(LocaleHack.supportedLocales)..add('en');
  group('getFlutterLocale', () {
    for (var locale in locales) {
      testWidgets(locale, (WidgetTester tester) async {
        tester.binding.window.localesTestValue = [flutter_mat.Locale(locale)];
        tester.binding.window.localeTestValue = flutter_mat.Locale(locale);
        expect(
            LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
        if (LocaleHack.flutterLocaleRemap[locale] != null) {
          expect(LocaleHack.getFlutterLocale().toString(),
              LocaleHack.flutterLocaleRemap[locale]);
        } else {
          expect(LocaleHack.getFlutterLocale().toString(), locale);
        }
      });
    }
    testWidgets('explicitly: nn', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nn')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nn');
      expect(LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
      expect(LocaleHack.getFlutterLocale().toString(), 'nb');
    });
    testWidgets('explicitly: nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
      expect(LocaleHack.getFlutterLocale().toString(), 'nb');
    });
  });
  group('detectLocale', () {
    for (var locale in locales) {
      testWidgets(locale, (WidgetTester tester) async {
        tester.binding.window.localesTestValue = [flutter_mat.Locale(locale)];
        tester.binding.window.localeTestValue = flutter_mat.Locale(locale);
        expect(LocaleHack.detectLocale(), TypeMatcher<String>());
        expect(LocaleHack.detectLocale(), locale);
      });
    }
    testWidgets('explicitly: nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.detectLocale(), TypeMatcher<String>());
      expect(LocaleHack.detectLocale(), 'nn');
    });
  });
  testWidgets('intlInit', (WidgetTester tester) async {
    expect(Intl.defaultLocale, isNull);
    LocaleHack.intlInit();
    expect(Intl.defaultLocale, LocaleHack.detectLocale());
  });
  group('dateLocale', () {
    for (var locale in locales) {
      testWidgets(locale, (WidgetTester tester) async {
        tester.binding.window.localesTestValue = [flutter_mat.Locale(locale)];
        tester.binding.window.localeTestValue = flutter_mat.Locale(locale);
        expect(LocaleHack.dateLocale, TypeMatcher<String>());
        if (LocaleHack.dateLocaleMap[locale] != null) {
          expect(LocaleHack.dateLocale, LocaleHack.dateLocaleMap[locale]);
        } else {
          expect(LocaleHack.dateLocale, locale);
        }
      });
    }
    testWidgets('explicit: nn', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nn')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nn');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'no_NO');
    });
    testWidgets('explicit: nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'no_NO');
    });
    testWidgets('explicit: fi', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('fi')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('fi');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'fi');
    });
  });
}
