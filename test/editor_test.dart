// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/editor.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/genericwidgets.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mockito/mockito.dart';
import 'utils.dart';

void main() {
  setTestFile('editor');

  group('MigraineNoteEditor', () {
    testWidgets('golden', (WidgetTester tester) async {
      await goldenTest(
        widgetType: MigraineNoteEditor,
        widgetInstance: MigraineNoteEditor(entry: MigraineEntry()),
        tester: tester,
      );
    });
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      await tester.pumpWidget(materialWidget(MigraineNoteEditor(entry: e)));
      await tester.enterText(find.byType(TextField), 'Hello World');
      expect(e.note, "Hello World");
    });
  });
  group('MigraineMedsTakenSelector', () {
    var dummyMeds = ['Test1', 'Test2'];
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      e.medications.addList(dummyMeds);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: MigraineLogConfig()),
        ],
      ));
      await goldenTest(
        widgetType: MigraineMedsTakenSelector,
        tester: tester,
        name: "MigraineMedsTakenSelector",
      );
    });
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      e.medications.add('Custom medication');
      var c = MigraineLogConfig();
      c.medications.addList(dummyMeds);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(
          e.medications.has(MigraineMedicationEntry(medication: dummyMeds[0])),
          isFalse);
      expect(find.text('Custom medication'), findsOneWidget,
          reason: 'The custom medication should have been displayed');
      // All config meds should be displayed
      for (var dummy in dummyMeds) {
        expect(find.text(dummy), findsOneWidget);
      }
      // Tapping a med should work
      await tester.tap(find.text(dummyMeds[0]));
      await tester.pump();
      expect(
          e.medications.has(MigraineMedicationEntry(medication: dummyMeds[0])),
          isTrue);
      // Tapping again to remove it should work
      await tester.tap(find.text(dummyMeds[0]));
      await tester.pump();
      expect(
          e.medications.has(MigraineMedicationEntry(medication: dummyMeds[0])),
          isFalse);
      // Tapping the second one should work
      expect(
          e.medications.has(MigraineMedicationEntry(medication: dummyMeds[1])),
          isFalse);
      await tester.tap(find.text(dummyMeds[1]));
      await tester.pump();
      expect(
          e.medications.has(MigraineMedicationEntry(medication: dummyMeds[1])),
          isTrue);
      // Tapping the custom one should remove it
      await tester.tap(find.text('Custom medication'));
      await tester.pump();
      expect(
          e.medications
              .has(MigraineMedicationEntry(medication: 'Custom medication')),
          isFalse);
      expect(find.text('Custom medication'), findsNothing);
      await tester.flush();
    });
  });
  group('MigraineStrengthSelector', () {
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineStrengthSelector,
        tester: tester,
        name: "MigraineStrengthSelector",
      );
    });
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      Finder selected =
          find.byWidgetPredicate((w) => w is RadioListTile && w.checked);
      expect(selected, findsNothing);
      expect(e.strength, isNull);

      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.strongMigraine);

      await tester.tap(find.text(c.messages.migraineMesssage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.migraine);

      await tester.tap(find.text(c.messages.headacheMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.headache);

      await tester.flush();
    });
    testWidgets('Help golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      await goldenTest(
        widgetType: AlertDialog,
        tester: tester,
        name: "MigraineStrengthSelector.help",
      );
    });
    testWidgets('Help functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      expect(find.byType(AlertDialog), findsOneWidget);
      expect(find.text('CLOSE'), findsOneWidget);
      await tester.tap(find.text('CLOSE'));
      await tester.pump();
      expect(find.byType(AlertDialog), findsNothing);
    });
  });
  group('MigraineDateSelector', () {
    testWidgets('golden', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 2, 19);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await goldenTest(
        widgetType: MigraineDateSelector,
        tester: tester,
        name: "MigraineDateSelector",
      );
    });
    testWidgets('Functionality', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      await tester.tap(find.byWidgetPredicate((w) => w is RaisedButton));
      await tester.pump();
      expect(find.byWidgetPredicate((w) => w is CalendarDatePicker),
          findsOneWidget);
      await tester.tap(find.text("2"));
      await tester.pump();
      await tester.tap(find.text("OK"));
      await tester.pump();
      expect(dateString(e.date), dateString(DateTime(2021, 1, 2)));
    });
    testWidgets('Edit warning', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(editMode: DateTime.now()),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      var warning = RegExp(r"You're editing");
      expect(
          find.byWidgetPredicate(
              (w) => w is MDTextBox && warning.hasMatch(w.text)),
          findsOneWidget);
    });
    testWidgets('Overwrite warning', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      var e2 = MigraineEntry(parentList: l);
      e2.date = DateTime(2021, 1, 1);
      l.set(e2);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      var warning = RegExp(r"Warning");
      expect(
          find.byWidgetPredicate(
              (w) => w is MDTextBox && warning.hasMatch(w.text)),
          findsOneWidget);

      await tester.flush();
    });
  });
  group('MigraineLogEditor', () {
    testWidgets('golden', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 2, 19);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogEditor,
        tester: tester,
        name: "MigraineLogEditor",
      );
    });
    testWidgets('Saving without strength', (WidgetTester tester) async {
      var navO = MockNavigatorObserver();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MockMigraineList();
      when(l.exists(any)).thenReturn(false);
      var e = MigraineEntry(parentList: l);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        navObserver: navO,
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsNothing);
      await tester.tap(find.byIcon(Icons.save));
      await tester.pump();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget,
          reason: 'SnackBar with error message should be displayed');
      verifyNever(navO.didPop(any, any));
      verifyNever(l.set(any));
    });
    testWidgets('Saving', (WidgetTester tester) async {
      var navO = MockNavigatorObserver();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MockMigraineList();
      when(l.exists(any)).thenReturn(false);
      var e = MigraineEntry(parentList: l);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        navObserver: navO,
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsNothing);
      await tester.tap(find.byIcon(Icons.save));
      await tester.pump();
      verify(navO.didPop(any, any));

      verify(l.set(e));

      await tester.flush();
    });
  });
}

class MockMigraineList extends Mock implements MigraineList {}
