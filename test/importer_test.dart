// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/importer.dart';
import 'package:MigraineLog/exporter.dart';
import 'package:flutter/material.dart' hide TypeMatcher;
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:matcher/matcher.dart';
import 'dart:io';
import 'utils.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  Directory tmp;

  setUp(() async {
    await initializeDateFormatting("en", null);
    tmp = await Directory.systemTemp.createTemp();
    return true;
  });
  tearDown(() async {
    tmp.deleteSync(recursive: true);
    tmp = null;
    return true;
  });

  group('importFromString', () {
    test('valid', () async {
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.version = 'testing';

      var newL = MigraineList(config: c);
      var l = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var h = MigraineLogExporter(l, config: c, state: s);

      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.headache;
      e.note = 'Testnote';
      e.date = DateTime(2021, 1, 1);
      e.medications.add('Medication');

      var e2 = MigraineEntry(parentList: l);
      e2.strength = MigraineStrength.migraine;
      e2.note = 'Testnote';
      e2.date = DateTime(2021, 1, 5);
      e2.medications.add('Medication');

      var e3 = MigraineEntry(parentList: l);
      e3.strength = MigraineStrength.strongMigraine;
      e3.date = DateTime(2021, 1, 9);

      l.set(e);
      l.set(e2);
      l.set(e3);

      expect(newL.isNotEmpty, isFalse);
      expect(newL.exists(e.date), isFalse);
      expect(newL.exists(e2.date), isFalse);
      expect(newL.exists(e3.date), isFalse);

      var r = i.importFromString(await h.get());
      expect(r, TypeMatcher<MigraineLogImporterResult>());
      expect(r.success, isTrue);
      expect(r.error, isNull);
      expect(newL.isNotEmpty, isTrue);

      expect(newL.exists(e.date), isTrue);
      expect(newL.get(e.date).strength, e.strength);
      expect(newL.get(e.date).note, e.note);
      expect(
          newL.get(e.date).medications.has(e.medications.list.first), isTrue);

      expect(newL.exists(e2.date), isTrue);
      expect(newL.get(e2.date).strength, e2.strength);
      expect(newL.get(e2.date).note, e2.note);
      expect(
          newL.get(e2.date).medications.has(e2.medications.list.first), isTrue);

      expect(newL.exists(e3.date), isTrue);
      expect(newL.get(e3.date).strength, e3.strength);
      expect(newL.get(e3.date).note, e3.note);
      expect(newL.get(e3.date).medications.isNotEmpty, isFalse);
    });
    test('overwriting', () async {
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.version = 'testing';

      var l = MigraineList(config: c);
      var i = MigraineLogImporter(l);
      var h = MigraineLogExporter(l, config: c, state: s);

      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.headache;
      e.note = 'Testnote';
      e.date = DateTime(2021, 1, 1);
      e.medications.add('Medication');
      l.set(e);

      expect(l.get(e.date).note, 'Testnote');
      var exported = await h.get();
      e.note = 'TESTING';
      expect(l.get(e.date).note, 'TESTING');
      var r = i.importFromString(exported);
      expect(r, TypeMatcher<MigraineLogImporterResult>());
      expect(r.success, isTrue);
      expect(r.error, isNull);

      expect(l.get(e.date).note, 'Testnote');
    });
    test('empty, but valid', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var i = MigraineLogImporter(l);
      var result = i.importFromString(
          '<!DOCTYPE html>\nmigraineLogData\n{ "data": {}, "exportVersion":1}');
      expect(result.error, isNull);
      expect(result.success, isTrue);
    });
    test('invalid', () async {
      var c = MigraineLogConfig();
      var newL = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var result = i.importFromString('');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.importFromString('<!DOCTYPE html>');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.importFromString('migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.importFromString('<!DOCTYPE html> migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);

      result =
          i.importFromString('<!DOCTYPE html> migraineLogData\n{ false }\n');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);

      result = i.importFromString(
          '<!DOCTYPE html> migraineLogData\n{ "validJSON":true}\n');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);
      expect(result.error, "Data missing");

      result =
          i.importFromString('<!DOCTYPE html> migraineLogData\n{ "data":{}}\n');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
    });
    test('version number validation', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var i = MigraineLogImporter(l);
      var result = i.importFromString(
          '<!DOCTYPE html>\nmigraineLogData\n{ "data": {}, "exportVersion":2}');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
      result = i.importFromString(
          '<!DOCTYPE html>\nmigraineLogData\n{ "data": {}, "exportVersion":99999}');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
      result =
          i.importFromString('<!DOCTYPE html>\nmigraineLogData\n{ "data": {}}');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
      result = i.importFromString(
          '<!DOCTYPE html>\nmigraineLogData\n{ "data": {}, "exportVersion":0}');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
      result = i.importFromString(
          '<!DOCTYPE html>\nmigraineLogData\n{ "data": {}, "exportVersion":"a_string"}');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.unsupportedVersion);
    });
  });
  group('getJSONFromHTMLString', () {
    test('valid', () async {
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.version = 'testing';

      var newL = MigraineList(config: c);
      var l = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var h = MigraineLogExporter(l, config: c, state: s);

      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.headache;
      e.note = 'Testnote';
      e.date = DateTime(2021, 1, 1);
      e.medications.add('Medication');

      var e2 = MigraineEntry(parentList: l);
      e2.strength = MigraineStrength.migraine;
      e2.note = 'Testnote';
      e2.date = DateTime(2021, 1, 5);
      e2.medications.add('Medication');

      var e3 = MigraineEntry(parentList: l);
      e3.strength = MigraineStrength.strongMigraine;
      e3.date = DateTime(2021, 1, 9);

      l.set(e);
      l.set(e2);
      l.set(e3);

      var r = i.getJSONFromHTMLString(await h.get());
      expect(r, TypeMatcher<MLIIntermediateResult>());
      expect(r.success, isTrue);
      expect(r.error, isNull);
      expect(r.result.startsWith('{'), isTrue);
    });
    test('invalid', () async {
      var c = MigraineLogConfig();
      var newL = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var result = i.getJSONFromHTMLString('');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getJSONFromHTMLString('<!DOCTYPE html>');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getJSONFromHTMLString('migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getJSONFromHTMLString('<!DOCTYPE html> migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);

      result = i.getJSONFromHTMLString(
          '<!DOCTYPE html> migraineLogData\n{ false }\n');
      expect(result.success, isTrue);
      expect(result.result, '{ false }');
    });
  });
  group('getMapFromHTMLString', () {
    test('valid', () async {
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.version = 'testing';

      var newL = MigraineList(config: c);
      var l = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var h = MigraineLogExporter(l, config: c, state: s);

      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.headache;
      e.note = 'Testnote';
      e.date = DateTime(2021, 1, 1);
      e.medications.add('Medication');

      var e2 = MigraineEntry(parentList: l);
      e2.strength = MigraineStrength.migraine;
      e2.note = 'Testnote';
      e2.date = DateTime(2021, 1, 5);
      e2.medications.add('Medication');

      var e3 = MigraineEntry(parentList: l);
      e3.strength = MigraineStrength.strongMigraine;
      e3.date = DateTime(2021, 1, 9);

      l.set(e);
      l.set(e2);
      l.set(e3);

      var r = i.getMapFromHTMLString(await h.get());
      expect(r, TypeMatcher<MLIIntermediateResult>());
      expect(r.success, isTrue);
      expect(r.error, isNull);
      expect(r.result, TypeMatcher<Map>());
    });
    test('invalid', () async {
      var c = MigraineLogConfig();
      var newL = MigraineList(config: c);
      var i = MigraineLogImporter(newL);
      var result = i.getMapFromHTMLString('');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getMapFromHTMLString('<!DOCTYPE html>');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getMapFromHTMLString('migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.wrongFiletype);

      result = i.getMapFromHTMLString('<!DOCTYPE html> migraineLogData');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);

      result = i
          .getMapFromHTMLString('<!DOCTYPE html> migraineLogData\n{ false }\n');
      expect(result.success, isFalse);
      expect(result.errorType, MigraineLogImporterError.corruptFile);
    });
  });
  test('parseSingleEntry', () {
    var c = MigraineLogConfig();
    var newL = MigraineList(config: c);
    var i = MigraineLogImporter(newL);

    var date = DateTime(2021, 1, 1);

    var result = i.parseSingleEntry({
      "strength": MigraineStrength.migraine.humanNumeric,
      "note": 'note',
      "medicationList": ['taken'],
    }, date);
    expect(result, TypeMatcher<MigraineEntry>());
    expect(result.date, date);
    expect(result.note, 'note');
    expect(result.medications.has(MigraineMedicationEntry(medication: 'taken')),
        isTrue);
  });
  group('UI', () {
    testWidgets('No data', (WidgetTester tester) async {
      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );
      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.flush();
    });
    testWidgets('Functional data', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.version = 'testing';

      var l = MigraineList(config: c);
      var h = TestMLToHTML(l, config: c, state: s);

      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.headache;
      e.note = 'Testnote';
      e.date = DateTime(2021, 1, 1);
      e.medications.add('Medication');

      var e2 = MigraineEntry(parentList: l);
      e2.strength = MigraineStrength.migraine;
      e2.note = 'Testnote';
      e2.date = DateTime(2021, 1, 5);
      e2.medications.add('Medication');

      var e3 = MigraineEntry(parentList: l);
      e3.strength = MigraineStrength.strongMigraine;
      e3.date = DateTime(2021, 1, 9);

      l.set(e);
      l.set(e2);
      l.set(e3);

      var content = await h.get();

      File(tmpFile).writeAsStringSync(content);

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isTrue);

      await tester.flush();
    });
    testWidgets('Invalidly formatted', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';

      File(tmpFile).writeAsStringSync("<html>\n{ broken }\n</html>");

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget);
      expect(
          find.text(ui.importFailedMessage() + ' ' + ui.wrongFiletypeMessage()),
          findsOneWidget);

      await tester.flush();
    });
    testWidgets('Missing migraineLogData', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';

      File(tmpFile)
          .writeAsStringSync("<!DOCTYPE html>\n<html>\n{ broken }\n</html>");

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget);
      expect(
          find.text(ui.importFailedMessage() + ' ' + ui.wrongFiletypeMessage()),
          findsOneWidget);

      await tester.flush();
    });
    testWidgets('Invalid JSON', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';

      File(tmpFile).writeAsStringSync(
          "<!DOCTYPE html>\n<html>\nmigraineLogData\n{ broken }\n</html>");

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget);
      expect(
          find.text(ui.importFailedMessage() + ' ' + ui.corruptErrorMessage()),
          findsOneWidget);

      await tester.flush();
    });
    testWidgets('Missing data', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';

      File(tmpFile).writeAsStringSync(
          "<!DOCTYPE html>\n<html>\nmigraineLogData\n{ \"exportVersion\":9999999 }\n</html>");

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget);
      expect(
          find.text(ui.importFailedMessage() + ' ' + ui.corruptErrorMessage()),
          findsOneWidget);

      await tester.flush();
    });
    testWidgets('Wrong version', (WidgetTester tester) async {
      var tmpFile = tmp.path + '/file.html';

      File(tmpFile).writeAsStringSync(
          "<!DOCTYPE html>\n<html>\nmigraineLogData\n{ \"exportVersion\":9999999, \"data\":{}}\n</html>");

      var context;
      var list = MigraineList(config: MigraineLogConfig());
      var ui = TestUI(list);
      ui.filePath = tmpFile;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Builder(builder: (BuildContext c) {
              context = c;
              return Container();
            }),
          ),
        ),
      );

      expect(await ui.getFromFilepicker(context), isFalse);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget);
      expect(
          find.text(
              ui.importFailedMessage() + ' ' + ui.unsupportedVersionMessage()),
          findsOneWidget);

      await tester.flush();
    });
  });
}

class TestUI extends MigraineLogImportUI {
  TestUI(list) : super(list);

  String filePath;

  @override
  Future<String> initiateFilePicker() async {
    return filePath;
  }
}

class TestMLToHTML extends MigraineListToHTML {
  TestMLToHTML(
    list, {
    state,
    config,
  }) : super(list, state: state, config: config);

  @override
  Future<String> asset() async {
    return '';
  }
}
