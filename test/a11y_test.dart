// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:MigraineLog/main.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/viewer.dart';
import 'package:MigraineLog/config.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/stats.dart';
import 'package:flutter/material.dart';
import 'utils.dart';
import 'main_mocks.dart';

MigraineList globalList;
MigraineLogConfig globalConfig;

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  setTestFile('main_full');
  packageInfoMock();

  setUp(() async {
    globalConfig = PseudoMigraineLogConfig();
    globalList = PseudoMigraineList(config: globalConfig);
    return true;
  });

  group('Guidelines', () {
    testWidgets('home', (WidgetTester tester) async {
      globalConfig.onboardingVersion = 999;
      var str = 0;
      var now = DateTime.now();
      for (var n = 0; n < 8; n++) {
        if (str++ >= 3) {
          str = 1;
        }
        var e = MigraineEntry(parentList: globalList);
        e.date = now.subtract(Duration(days: n));
        e.strength = MigraineStrengthConverter.fromNumber(str);
        globalList.set(e);
      }
      await tester.pumpWidget(
        A11FakeMigraineLog(),
      );
      await tester.pumpAndSettle();

      expect(tester, meetsGuideline(androidTapTargetGuideline));
      expect(tester, meetsGuideline(textContrastGuideline));

      expect(find.byIcon(Icons.calendar_today), findsOneWidget);

      await tester.tap(find.byIcon(Icons.calendar_today));
      await tester.pump();
      await tester.flush();
    });
    testWidgets('calendar tab', (WidgetTester tester) async {
      globalConfig.onboardingVersion = 999;
      var str = 0;
      var now = DateTime.now();
      for (var n = 0; n < 8; n++) {
        if (str++ >= 3) {
          str = 1;
        }
        var e = MigraineEntry(parentList: globalList);
        e.date = now.subtract(Duration(days: n));
        e.strength = MigraineStrengthConverter.fromNumber(str);
        globalList.set(e);
      }
      await tester.pumpWidget(
        A11FakeMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.calendar_today), findsOneWidget);
      await tester.tap(find.byIcon(Icons.calendar_today));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogCalendarWidget), findsOneWidget);

      // No tap target test here, the calendar fails it. Not sure if it is
      // because the tap targets are actually smaller for single-digit items, or
      // if it's just not understanding that the entire circle is tappable
      expect(tester, meetsGuideline(textContrastGuideline));

      await tester.flush();
    });
    testWidgets('stats tab', (WidgetTester tester) async {
      globalConfig.onboardingVersion = 999;
      var str = 0;
      var now = DateTime.now();
      for (var n = 0; n < 8; n++) {
        if (str++ >= 3) {
          str = 1;
        }
        var e = MigraineEntry(parentList: globalList);
        e.date = now.subtract(Duration(days: n));
        e.strength = MigraineStrengthConverter.fromNumber(str);
        globalList.set(e);
      }
      await tester.pumpWidget(
        A11FakeMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.pie_chart), findsOneWidget);
      await tester.tap(find.byIcon(Icons.pie_chart));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogStatsViewer), findsOneWidget);

      expect(tester, meetsGuideline(androidTapTargetGuideline));
      expect(tester, meetsGuideline(textContrastGuideline));

      await tester.flush();
    });
    testWidgets('settings', (WidgetTester tester) async {
      globalConfig.onboardingVersion = 999;
      var str = 0;
      var now = DateTime.now();
      for (var n = 0; n < 8; n++) {
        if (str++ >= 3) {
          str = 1;
        }
        var e = MigraineEntry(parentList: globalList);
        e.date = now.subtract(Duration(days: n));
        e.strength = MigraineStrengthConverter.fromNumber(str);
        globalList.set(e);
      }
      await tester.pumpWidget(
        A11FakeMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.more_vert), findsOneWidget);
      expect(find.text('Settings'), findsNothing);

      await tester.tap(find.byIcon(Icons.more_vert));
      await tester.pumpAndSettle();
      expect(find.text('Settings'), findsOneWidget);
      await tester.tap(find.text('Settings'));
      await tester.pumpAndSettle();

      expect(tester, meetsGuideline(androidTapTargetGuideline));
      expect(tester, meetsGuideline(textContrastGuideline));

      await tester.flush();
    });
    testWidgets('onboarding', (WidgetTester tester) async {
      globalConfig.onboardingVersion = 0;
      await tester.pumpWidget(
        A11FakeMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogNeutralLanguageOnboarding), findsOneWidget);
      await tester.pumpAndSettle();

      expect(tester, meetsGuideline(androidTapTargetGuideline));
      expect(tester, meetsGuideline(textContrastGuideline));

      await tester.flush();
    });
  });
}

class A11FakeMigraineLog extends MigraineLog {
  @override
  A11FakeMigraineLogState createState() => A11FakeMigraineLogState();
}

class A11FakeMigraineLogState extends MigraineLogState {
  @override
  // Initialize our MigraineList instance
  void initState() {
    super.initState();
    list = globalList;
    config = globalConfig;
  }
}

class PseudoMigraineLogConfig extends MigraineLogConfig {
  @override
  Future<bool> loadConfig() async {
    return true;
  }
}

class PseudoMigraineList extends MigraineList {
  PseudoMigraineList({config}) : super(config: config);
  @override
  Future<FileLoadStatus> loadData() async {
    return FileLoadStatus.success;
  }
}
