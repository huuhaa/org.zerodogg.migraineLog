// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/exporter.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:flutter_test/flutter_test.dart' show TestWidgetsFlutterBinding;
import 'package:share/share.dart';

void main() {
  // Needed for the getApplicationDocumentsDirectory mock to work.
  TestWidgetsFlutterBinding.ensureInitialized();

  MockMethodChannel mockChannel;

  setUp(() async {
    mockChannel = MockMethodChannel();
    Share.channel.setMockMethodCallHandler((MethodCall call) async {
      await mockChannel.invokeMethod<void>(call.method, call.arguments);
    });
    return true;
  });

  test('MigraineListToExternalJSON', () async {
    await initializeDateFormatting("en", null);
    var c = MigraineLogConfig();
    var s = MigraineLogGlobalState();
    s.version = 'testing';

    var l = MigraineList(config: c);
    var h = MigraineListToExternalJSON(l, config: c, state: s);

    var e = MigraineEntry(parentList: l);
    e.strength = MigraineStrength.headache;
    e.note = 'Testnote';
    e.date = DateTime(2021, 1, 1);
    e.medications.add('Medication');

    var e2 = MigraineEntry(parentList: l);
    e2.strength = MigraineStrength.migraine;
    e2.note = 'Testnote';
    e2.date = DateTime(2021, 1, 5);
    e2.medications.add('Medication');

    var e3 = MigraineEntry(parentList: l);
    e3.strength = MigraineStrength.strongMigraine;
    e3.date = DateTime(2021, 1, 9);

    l.set(e);
    l.set(e2);
    l.set(e3);

    expect(h.getMonthStats(DateTime(2021, 1, 1)), {
      "headacheDays": "3 days",
      "medicationDays": "2 days",
      "specificSummaries": {
        "Strong migraine": "1 day",
        "Migraine": "1 day",
        "Headache": "1 day"
      }
    });

    expect(h.getJSON(),
        '{"exportVersion":1,"version":"testing","data":{"202101":{"2021-01-01":{"strength":1,"note":"Testnote","medicationList":["Medication"],"date":"Friday, 1. January 2021"},"2021-01-05":{"strength":2,"note":"Testnote","medicationList":["Medication"],"date":"Tuesday, 5. January 2021"},"2021-01-09":{"strength":3,"note":"","medicationList":[],"date":"Saturday, 9. January 2021"}}},"summaries":{"202101":{"headacheDays":"3 days","medicationDays":"2 days","specificSummaries":{"Strong migraine":"1 day","Migraine":"1 day","Headache":"1 day"}}},"strings":{"messages":{"migraineLog":"Migraine Log","note":"Note","none":"(none)","strength":"Strength","date":"Date","takenMeds":"Medications","totalHeadacheDays":"Total headache days","hide":"hide","hideTooltip":"Hide this month","hidden":"hidden:","months":"months","everything":"everything","show":"show","filter":"Filter:","listEmptyDays":"List days with no entries","dateStrings":{"weekdays":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],"months":["January","February","March","April","May","June","July","August","September","October","November","December"]}},"strength":{"2":"Migraine","3":"Strong migraine","1":"Headache"},"months":{"202101":"January 2021"}}}');
  });

  test('MigraineListToHTML', () async {
    await initializeDateFormatting("en", null);
    var c = MigraineLogConfig();
    var s = MigraineLogGlobalState();
    s.version = 'testing';

    var l = MigraineList(config: c);
    var h = MigraineListToHTML(l, config: c, state: s);

    var e = MigraineEntry(parentList: l);
    e.strength = MigraineStrength.headache;
    e.note = 'Testnote';
    e.date = DateTime(2021, 1, 1);
    e.medications.add('Medication');

    var e2 = MigraineEntry(parentList: l);
    e2.strength = MigraineStrength.migraine;
    e2.note = 'Testnote';
    e2.date = DateTime(2021, 1, 5);
    e2.medications.add('Medication');

    var e3 = MigraineEntry(parentList: l);
    e3.strength = MigraineStrength.strongMigraine;
    e3.date = DateTime(2021, 1, 9);

    l.set(e);
    l.set(e2);
    l.set(e3);

    var html = await h.get();
    expect(html, startsWith('<!DOCTYPE html>'));
    expect(html, contains("migraineLogData"));
    expect(html, contains("{\"exportVersion\":1"));
    expect(html, contains('GNU General Public License'));

    expect(
        html,
        contains(
            '{"exportVersion":1,"version":"testing","data":{"202101":{"2021-01-01":{"strength":1,"note":"Testnote","medicationList":["Medication"],"date":"Friday, 1. January 2021"},"2021-01-05":{"strength":2,"note":"Testnote","medicationList":["Medication"],"date":"Tuesday, 5. January 2021"},"2021-01-09":{"strength":3,"note":"","medicationList":[],"date":"Saturday, 9. January 2021"}}},"summaries":{"202101":{"headacheDays":"3 days","medicationDays":"2 days","specificSummaries":{"Strong migraine":"1 day","Migraine":"1 day","Headache":"1 day"}}},"strings":{"messages":{"migraineLog":"Migraine Log","note":"Note","none":"(none)","strength":"Strength","date":"Date","takenMeds":"Medications","totalHeadacheDays":"Total headache days","hide":"hide","hideTooltip":"Hide this month","hidden":"hidden:","months":"months","everything":"everything","show":"show","filter":"Filter:","listEmptyDays":"List days with no entries","dateStrings":{"weekdays":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],"months":["January","February","March","April","May","June","July","August","September","October","November","December"]}},"strength":{"2":"Migraine","3":"Strong migraine","1":"Headache"},"months":{"202101":"January 2021"}}}'));
  });

  test('MigraineLogExporter', () async {
    Directory tmp = await Directory.systemTemp.createTemp();
    PathProviderPlatform.instance = MockPathProviderPlatform(dir: tmp);

    await initializeDateFormatting("en", null);
    var c = MigraineLogConfig();
    var s = MigraineLogGlobalState();
    s.version = 'testing';

    var l = MigraineList(config: c);
    var h = MigraineLogExporter(l, config: c, state: s);

    var e = MigraineEntry(parentList: l);
    e.strength = MigraineStrength.headache;
    e.note = 'Testnote';
    e.date = DateTime(2021, 1, 1);
    e.medications.add('Medication');

    var e2 = MigraineEntry(parentList: l);
    e2.strength = MigraineStrength.migraine;
    e2.note = 'Testnote';
    e2.date = DateTime(2021, 1, 5);
    e2.medications.add('Medication');

    var e3 = MigraineEntry(parentList: l);
    e3.strength = MigraineStrength.strongMigraine;
    e3.date = DateTime(2021, 1, 9);

    l.set(e);
    l.set(e2);
    l.set(e3);

    await h.export();

    var file = File(tmp.path + '/migrainelog.html');

    verify(mockChannel.invokeMethod('shareFiles', any));

    expect(await file.exists(), isTrue);

    var content = file.readAsStringSync();
    expect(content, startsWith('<!DOCTYPE html>'));
    expect(content, contains("migraineLogData"));
    expect(content, contains('GNU General Public License'));

    tmp.deleteSync(recursive: true);
  });
}

class MockPathProviderPlatform extends Mock
    with MockPlatformInterfaceMixin
    implements PathProviderPlatform {
  MockPathProviderPlatform({this.dir});
  Directory dir;

  @override
  Future<String> getApplicationDocumentsPath() async {
    return dir.path;
  }
}

class MockMethodChannel extends Mock implements MethodChannel {}
