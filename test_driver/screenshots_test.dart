// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// NOTE: This file is not actually a test. This generates screenshots for the
// app. Use the 'screenshots' make target, which ties this file,
// screenshots.dart and scripts/prepareForScreenshots together to generate
// screenshots, one for each locale.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'dart:io';

void main() {
  group('MigraineLog', () {
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        await driver.close();
      }
    });

    test('main screen', () async {
      var screenshot = await driver.screenshot();
      await File('ss01main.png').writeAsBytes(screenshot);
    });
    test('calendar', () async {
      await driver.tap(find.byValueKey("calendar_tab"));
      var screenshot = await driver.screenshot();
      await File('ss02calendar.png').writeAsBytes(screenshot);
    });
    test('add/edit', () async {
      await driver.tap(find.byValueKey("home_tab"));
      await driver.tap(find.byValueKey("fab_add_button"));
      var screenshot = await driver.screenshot();
      await File('ss03addedit.png').writeAsBytes(screenshot);
    });
  });
}
