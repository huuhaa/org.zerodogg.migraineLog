#!/usr/bin/env dart
// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:io';
import 'dart:convert';
import 'package:path/path.dart' as p;

void die (String message)
{
  print(message);
  exit(1);
}

Future<String> slurp(String fileName) async
{
  var inFile = File(fileName);
  if(!inFile.existsSync())
  {
    die("$fileName does not exist");
  }
  String content = await inFile.readAsString();
  return content;
}

void main (List<String> arguments) async
{
  exitCode = 0;

  final String usage = "Usage: ./scripts/prepareForScreenshots LOCALE\nYou should probably just use the 'screenshots' makefile target though.";

  if(arguments.length == 0)
  {
    die(usage);
  }

  var locale = arguments[0];

  if(locale != null)
  {
    locale = locale.replaceAll(RegExp('-.+'),'');
    if(locale == 'no')
    {
      locale = 'nn';
    }
    var data;
    var file = "./i18n/$locale.arb";
    if(locale == "en")
    {
      data = {
        "Painkillers":"Painkillers",
        "Migraine medication":"Migraine medication",
      };
    }
    else
    {
      data = jsonDecode(await slurp(file));
    }
    String source = await slurp('./test_driver/screenshots.dart');
    String out = source.replaceAll("MIGRAINE_MEDICATION",data['Migraine medication']).replaceAll("PAINKILLERS",data['Painkillers']).replaceAll("OVERRIDE_LOCALE",locale);
    await File('./test_driver/screenshots-built.dart').writeAsString(out);
    await File('./test_driver/screenshots-built_test.dart').writeAsString(await File("./test_driver/screenshots_test.dart").readAsString());
  }
  else
  {
    die(usage);
  }
}
